.PHONY: dev bash shell dbshell build fmt lint rm-db-volume revision upgrade downgrade merge_heads

COMPOSE-DEV = docker-compose -f docker-compose.yml

dev:
	if [ ! -f .env ]; then cp .env.example .env; fi;
	$(COMPOSE-DEV) up --build

bash:
	$(COMPOSE-DEV) exec falcon /bin/sh

shell:
	$(COMPOSE-DEV) exec falcon flask debug

dbshell:
	$(COMPOSE-DEV) exec falcon pgcli postgresql://postgres:password@falcon-db:5432/falcon

build:
	$(COMPOSE-DEV) build

revision:
	$(COMPOSE-DEV) exec falcon alembic revision --autogenerate -m $(name)

upgrade:
	$(COMPOSE-DEV) exec falcon alembic upgrade head

downgrade:
	$(COMPOSE-DEV) exec falcon alembic downgrade -1

merge-heads:
	$(COMPOSE-DEV) exec falcon alembic merge heads -m merge_heads

fmt:
	black .

lint:
	flake8 .

rm-db-volume:
	docker stop falcon-db && docker rm falcon-db && docker volume rm falcon-api_falcon-db


