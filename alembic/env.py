from __future__ import with_statement
import os
import sys
import logging
from sqlalchemy.ext.declarative import as_declarative
import importlib

from pathlib import Path
import typing
from alembic import context
from sqlalchemy import engine_from_config, pool
from logging.config import fileConfig
from app.settings.settings import SQLALCHEMY_DATABASE_URI  # noqa


sys.path.append(".")

logger = logging.getLogger(__name__)


@as_declarative()
class Base:
    pass


def import_mods(
    mod_name: str,
    sub_apps=None,
    app_dir: str = "app",
) -> typing.Iterable[tuple[str, typing.Any]]:
    app_path = app_dir.replace("/", ".")

    if not sub_apps:
        sub_apps = [a for a in os.listdir(app_dir) if not a.startswith((".", "_"))]

    print("> Scan sub_apps:")
    for sub_app in sub_apps:
        print(">   sub_app: %s" % sub_app)
        exists = (Path(app_dir) / sub_app / f"{mod_name}.py").exists()
        exists_package = (Path(app_dir) / sub_app / f"{mod_name}").exists()
        if exists:
            path = f"{app_path}.{sub_app}.{mod_name}"
            yield (sub_app, importlib.import_module(path))
        elif exists_package:
            path = f"{app_path}.{sub_app}.{mod_name}"
            yield (sub_app, importlib.import_module(path))
        else:
            logger.debug(
                "No mod: %s in app: %s.%s",
                mod_name,
                sub_app,
                app_path,
            )


def import_models():
    res = [m for _, m in import_mods(mod_name="models")]
    print("> Collect models:")
    for r in res:
        print(">   model: %s" % r)
    return res

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
fileConfig(config.config_file_name)

# add your model's MetaData object here
# for 'autogenerate' support
# from myapp import mymodel
# target_metadata = mymodel.Base.metadata
# target_metadata = None

target_metadata = Base.metadata

# other values from the config, defined by the needs of env.py,
# can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.

# Import all models
import_models()


def run_migrations_offline():
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script outputyping.

    """
    url = SQLALCHEMY_DATABASE_URI
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        compare_type=True
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    configuration = config.get_section(config.config_ini_section)
    configuration["sqlalchemy.url"] = SQLALCHEMY_DATABASE_URI
    connectable = engine_from_config(
        configuration,
        prefix="sqlalchemy.",
        poolclass=pool.NullPool,
    )

    with connectable.connect() as connection:
        context.configure(
            connection=connection, target_metadata=target_metadata, compare_type=True
        )

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
