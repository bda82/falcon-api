import typing


class BaseException(Exception):
    def __init__(
        self,
        msg: str,
        http_status_code: typing.Optional[int] = None,
        status_code: typing.Optional[int] = None,
        *args,
    ):
        super().__init__(msg, http_status_code, status_code, *args)
        self.msg = msg
        self.http_status_code = http_status_code
        self.error_code = status_code
