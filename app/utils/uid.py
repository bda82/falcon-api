import typing
import uuid


def get_random_uuid(as_str=True) -> typing.Union[str, uuid.UUID]:
    res = uuid.uuid4()
    if as_str:
        return str(res)
    return res
