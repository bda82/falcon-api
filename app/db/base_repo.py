import typing
import logging
from datetime import datetime

from abc import ABC

from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session, Query
from sqlalchemy.sql import select, Select
from sqlalchemy import func

from app.middleware.base_exception import BaseException

logger = logging.getLogger(__name__)

T = typing.TypeVar("T")


class RepositoryException(BaseException):
    """"""


class PaginatedList(list):
    def __init__(self, *args, query: Query = None, total: int = 0):
        self.query = query
        total_ = total if total != 0 else query.count() if query else total
        self.total = total_
        super(PaginatedList, self).__init__(*args)


class BaseRepo(ABC):
    model: type(T) = NotImplemented
    _logger = logger

    def __init__(self, db: Session):
        self.db = db
        self.name = self.model.__name__

    @staticmethod
    def is_null_error(e: Exception):
        return "null value in column" in str(e)

    @staticmethod
    def is_check_constraint_error(e: Exception):
        return "violates check constraint" in str(e)

    @staticmethod
    def check_constraint_error(e: Exception):
        return "violates check constraint" in str(e)

    def mutate(self, **kwargs) -> dict:
        return kwargs

    def save(self, obj: T) -> T:
        self.db.add(obj)
        try:
            self.db.commit()
        except IntegrityError as e:
            self.db.rollback()
            if self.is_null_error(e):
                raise RepositoryException("Required fields are empty")
            if self.check_constraint_error(e):
                raise RepositoryException("Check constraint error")

            raise

        return obj

    def get(
        self, pk: typing.Union[int, str, list], silent: bool = True
    ) -> typing.Union[T, typing.Iterable[T], None]:
        if pk and isinstance(pk, list):
            res = self.db.query(self.model).filter(self.model.id.in_(pk)).all()
        elif pk:
            res = self.db.query(self.model).get(pk)
        else:
            raise RepositoryException("Empty id")

        if not (res or silent):
            raise RepositoryException(f"Does not exist {self.name}:{pk}")

        return res

    def delete(self, pk: typing.Union[int, str]):
        obj = self.get(pk, silent=False)
        self.db.delete(obj)
        try:
            self.db.commit()
        except IntegrityError as e:
            self.db.rollback()
            msg = f"Cannot delete {self.name}:{pk}"
            self._logger.warning(f"{msg} Original:{e}")
            raise RepositoryException(msg)

        self._logger.info("Deleted %s:%s", self.name, pk)

        return obj

    def create(self, **kwargs) -> T:
        data = self.mutate(**kwargs)
        obj = self.model(**data)

        self.save(obj)
        self.db.refresh(obj)

        self._logger.info("Created %s:%s", self.name, obj.id)

        return obj

    def update(
        self,
        pk: typing.Union[int, str],
        ignore_unset: bool = False,
        **kwargs,
    ) -> T:
        obj = self.get(pk, silent=False)

        if hasattr(obj, "updated_at"):
            obj.updated_at = datetime.utcnow()

        data = self.mutate(**kwargs)
        for field, value in data.items():
            if value is not None or not ignore_unset:
                setattr(obj, field, value)

        self.save(obj)

        self.db.refresh(obj)
        return obj

    def total(self) -> int:
        return self.db.query(self.model).count()

    def paginate(
        self, query: Query, sort_by: str, is_asc: bool, page: int, limit: int
    ) -> PaginatedList[T]:
        order_field = getattr(self.model, sort_by)
        if not is_asc:
            order_field = order_field.desc()
        offset = (page - 1) * limit
        if isinstance(query, Query):
            return PaginatedList(
                query.order_by(order_field).offset(offset).limit(limit).all(),
                query=query,
            )
        elif isinstance(query, Select):
            return PaginatedList(
                self.db.scalars(
                    query.order_by(order_field).offset(offset).limit(limit)
                ).all(),
                total=self.db.scalar(select(func.count()).select_from(query.subquery())),
            )