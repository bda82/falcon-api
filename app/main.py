import falcon

from app.todo.resources import TodoResource

app = falcon.API()
app.add_route('/', TodoResource())
