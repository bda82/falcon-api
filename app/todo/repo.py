from sqlalchemy import select

from app.db.base_repo import BaseRepo, PaginatedList

from app.todo.models import ToDo


class TodoRepo(BaseRepo):
    model = ToDo

    def get_multy(
        self,
        query: str = None,
        sort_by: str = "contact_fullname",
        is_asc: bool = True,
        page: int = 1,
        limit: int = 100,
        user_id: str = None,
    ) -> PaginatedList:
        q = select(self.model)

        if user_id is not None:
            q = q.where(self.model.user_id == user_id)

        if query:
            q = q.where(
                self.model.title.ilike(f"%{query}%"),
                self.model.description.ilike(f"%{query}%"),
            )

        return self.paginate(
            query=q, sort_by=sort_by, is_asc=is_asc, page=page, limit=limit
        )
