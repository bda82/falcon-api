from sqlalchemy import Column, String, Boolean
from sqlalchemy.dialects.postgresql import UUID

from app.utils.uid import get_random_uuid
from app.db.base import Base
from app.db.dated import Dated


class ToDo(Base, Dated):
    __tablename__ = "todo"

    id = Column(
        UUID(as_uuid=False),
        primary_key=True,
        default=get_random_uuid,
        index=True
    )

    title = Column(String(256), index=True)
    description = Column(String(256), index=False)
    done = Column("done", Boolean, default=False, index=False)
