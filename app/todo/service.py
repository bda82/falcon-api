import dataclasses

from sqlalchemy.orm import Session

from app.todo.repo import TodoRepo


@dataclasses.dataclass
class TodoService:
    repo: TodoRepo


def get_todo_service(db: Session):
    return TodoService(repo=TodoRepo(db))
