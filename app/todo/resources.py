import json

import falcon


class TodoResource:
    def on_get(self, req, resp):
        doc = {
            "todos": [
                {"title": "title", "description": "description", "done": False},
            ]
        }
        resp.body = json.dumps(doc, ensure_ascii=False)
        resp.status = falcon.HTTP_200
