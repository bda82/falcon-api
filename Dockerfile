FROM python:3.10-alpine

RUN apk update \
    && apk add build-base  postgresql-dev python3-dev libffi-dev openssl-dev \
           zlib-dev jpeg-dev git tini \
    && pip3 install --no-cache-dir --upgrade pip

COPY requirements.txt /app/requirements.txt

RUN pip3 install -r /app/requirements.txt --no-cache-dir \
    && apk del --purge build-base


WORKDIR /app
ADD . /app

ENV PYTHONPATH "${PYTHONPATH}:/app"

ENTRYPOINT ["gunicorn", "-b", "0.0.0.0:8000", "--reload", "app.main:app"]
